# Cloud Formation Nested Stacks

<br>
<br>

# Contents
- [Objective](#objective)
- [Steps](#Steps)
- [Relevant Links](#relevant-links)
- [Future Plans](#)

<br>
<br>

# Objective 
- [go to top](#contents)

Create a CF nested stack (Infra.yaml) that will create the following resources and deploy it from console:
- VPC
- Private subnet
- Public subnet
- Route tables (public, private)
- Internet Gateway
- Security Groups
    - Ingress rules for the SG groups

Deploy it via console and AWS CLI

<br>
<br>

# Architecture Diagram
![](images/)

<br>
<br>

# Steps
- [go to top](#contents)

- [ ] Create the individual stack [templates](stacks/) for each resource
- [ ] Create a [main](stacks/main.yaml) stack to deploy them 
```
Parameters: 
  s3Bucket:
    Type: String
    Description: s3 bucket that holds the templates
    Default: bucket name here!
```
- [ ] Add the name the s3 bucket you intend to upload the stack templates to [main.yaml](stacks/main.yaml)
- [ ] Upload the files to s3 and deploy the stack

## Via Console
- [ ] Got to Cloud Formation console > Create Stack > Template is ready > Upload a template file > Upload the [main.yaml](stacks/main.yaml) file > Choose a stack name > Select Azs > Create Stack.


## Via CLI
- [ ] Upload stack templates to s3
```
 aws s3 cp path-to-stacks-folder s3://enter-bucket-name-here --recursive
``` 
- [ ] Create Cloud formation template
```
aws cloudformation create-stack --stack-name enter-stack-name-here --template-body file://path-to-main.yaml
```

<br>
<br>

# Relevant Links
- [go to top](#contents)
- [AWS CloudFormation Workshop](https://cfn101.workshop.aws)
- [Generate Cloud Formation Templates](https://asecure.cloud/l/s_vpc/)
- VS Code Cloud Formation Linter : [Here](https://github.com/aws-cloudformation/cfn-lint) and [Here](https://marketplace.visualstudio.com/items?itemName=kddejong.vscode-cfn-lint)
- [Cloud Formation Quotas](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/cloudformation-limits.html)
- [Intrinsic Function](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/intrinsic-function-reference.html)
- [CF VPC Template](https://docs.aws.amazon.com/codebuild/latest/userguide/cloudformation-vpc-template.html)
- [CF Create Stack](https://docs.aws.amazon.com/cli/latest/reference/cloudformation/create-stack.html)
- [Passing parameters as a Json file](https://scriptcrunch.com/error-parsing-parameter/), [and here](https://stackoverflow.com/questions/45749424/passing-multiple-parameters-from-external-file-to-cloudformation-template-and-us)


<br>
<br>

# Future Plans
- [go to top](#contents)

- [ ] Mappings, Terraform, Paramerters, Layered Stacks, Lambda Functions, Session manager
