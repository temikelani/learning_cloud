# Exchange rate EC2 to s3 Dump

<br>
<br>

# Contents
- [Objective](#objective)
- [Steps](#Steps)
- [Relevant Links](#relevant-links)
- [Future Plans](#)

<br>
<br>

# Objective 
- [go to top](#contents)

- I am starting an Exchange Company, and I want to get the `rate at which the exchange rate fluctuates` from USD to (insert your currency of choice) 
- I want to get an export of the exchange rate as a JSON output `every hour` into an `S3 bucket`. 
- I also want to be `notified once` this is completed. 

Your solution should have:
- [ ] An EC2: For running a python script that will generate exchange rates and write the output into a JSON file and send that JSON file into s3(this must be done hourly)
- [ ] An s3 bucket to store JSON files of the exchange rate 
- [ ] An IAM role, EC2 will need permission to write into the bucket where exchange rate JSON files will be stored.
- [ ] An SNS Topic, to send a job failure or job success notification to the exchange team each time you successfully write into the bucket or each time the jobs fails.
    - JOB failure will mean error encountered in the process any point in time. (simulate this as well)

<br>
<br>

# Architecture Diagram
![](images/)

<br>
<br>

# Steps
- [go to top](#contents)

- [ ] Create a `keypair` and `security group` allowing incoming `ssh, http, https` access. Limit ssh access to your personal IP
- [ ] Create an `EC2 Instance` with the `keypair` and `security group` created earlier. add a `Name tag`
- [ ] Create an `s3 bucket` in the `same region`
- [ ] Create an `SNS Topic` in the `same region` to send email notifications
- [ ] Create an [`IAM role`](iam) for the `EC2 Instance ` 
    - [ ] attach the policies for writing in s3 and publishing SNS messages
- [ ] ssh into the instance
- [ ] `pip3 insall forex_python, boto3`
- [ ] Create a [`directory`](exchange-rate-gen) to hold your scripts and data
    - [ ] add the [python script](exchange-rate-gen/exchange-rate-gen.py), create a `data directory` to hold the date and add the [shell script](exchange-rate-gen/script.sh)
    - [ ] edit the paths to the respective full paths in your `EC2 Instance`
    - `chmod +x` all scripts so you can execute them
- [ ] Add a `cronjob` to run the `shell script` that runs the `python script` every hour.

## Coming Soon: Via CLI / Via Console / Via CloudFormation
- [ ] 


<br>
<br>

# Relevant Links
- [go to top](#contents)
- [Forex Python Module](https://pypi.org/project/forex-python/)
- [Forex Python Tutorial](https://pythonawesome.com/a-free-foreign-exchange-rates-and-currency-conversion-in-python/)
- [Create an Ec2 instance](https://awscli.amazonaws.com/v2/documentation/api/latest/reference/ec2/run-instances.html)
- [Python DateTime](https://www.w3schools.com/python/python_datetime.asp)
- [s3 Boto3](https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/s3.html#S3.Client.upload_file)
- Crontab: [Here](https://phoenixnap.com/kb/set-up-cron-job-linux), [Here](https://serverfault.com/questions/449651/why-is-my-crontab-not-working-and-how-can-i-troubleshoot-it), [Here](https://phoenixnap.com/kb/crontab-reboot)
- [Coming: How upload files to ec2](#)

<br>
<br>

# Future Plans
- [go to top](#contents)

- [ ] Automate this with one cli shell run from my local pc. 
- [ ] Automate this with cloud formation?


