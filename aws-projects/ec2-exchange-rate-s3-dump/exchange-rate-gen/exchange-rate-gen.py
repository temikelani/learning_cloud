import json
import boto3
import requests
import forex_python
import time
from forex_python.converter import CurrencyRates
from datetime import datetime


# define aws resource clients
s3_client = boto3.client('s3', region_name='us-east-2')
s3_resource = boto3.resource('s3', region_name='us-east-2')
sns_client = boto3.client('sns', region_name='us-east-2')

try:
    # get usd currency rates in json format
    rates = CurrencyRates().get_rates('USD')
    rates = json.dumps(rates)

    # get current time
    time_now = datetime.now().strftime("%m-%d-%y_%H:%M:%S")

    # write the results to a new file
    fileName = f'/home/ec2-user/exchange-rate-gen/data/{time_now}_hourly-rates.json'

    with open(fileName, 'w') as ratesFile:
        ratesFile.write(rates)
        #json.dump(rates, ratesFile)
    ratesFile.close()

    # upload object in s3
    s3_resource.meta.client.upload_file(fileName, 'temi-exchange-rate-dump', f'{time_now}_hourly-rates.json')

except Exception as error:
    #send a message to the sns subscribers
    sns_response = sns_client.publish(
        TargetArn='enter-sns-topic-arn-here',
        Message= f'There was a failure when trying to upload exchange rates to s3: {error}',
        Subject='Exchange Dump Failed'
    )