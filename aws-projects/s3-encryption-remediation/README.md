# s3 bucket encryption remediation w Config, Lambda & SNS

<br>
<br>

# Contents
- [Objective](#objective)
- [Steps](#Steps)
- [Relevant Links](#relevant-links)
- [Future Plans](#)

<br>
<br>

# Objective 
- [go to top](#contents)

Your Senior Management is concerned about engineers creating ***unencrypted S3 buckets*** and uploading **unencrypted objects** into S3 buckets. They want you to ***track situations like this*** and if you find any, they want to **remediate them*** using ***Lambda***. 

After the **remediation is complete***, they want you to **send a notification to the security department** about ***which buckets were remediated.*** 

- You determine what your Lambda function should do.

Your solution should:
- [x] Detect any unencrypted S3 Buckets (Hint: Use AWS Config)
- [x] Remediate the bucket(s) and make them encrypted.
- [x] Send an SNS notification of which bucket(s) were remediated.

- Remediation scope is limited to automatically enabling default encryption in unencrypted s3 buckets 

<br>
<br>

# Architecture Diagram
![](images/S3_encryption_remediation.drawio.png)

<br>
<br>

# Steps
- [go to top](#contents)

## Via Console

- [x] Create an `AWS Config Rule` to check for encryption in s3 buckets.
    - [x] Choose the rule `s3-bucket-server-side-encryption-enabled` 

- [x] Create 1 bucket with default encryption disabled (SSE-S3): you'll use this to test your function
    - [x] Confirm your `Config Rule` works, check for a non-compliant status on your s3 bucket

- [x] Create a `Lambda function` from scratch (see code [here](lambda_functions/config-lambda-s3-encryption-remediaiton.py))

- [x] Create an `IAM Policy` (read action - `GetComplianceDetailsByConfig_rule`) allowing `Lambda` read the results of the `Config rule` above and attach it to the `role` automatically created for `Lambda`

- [x] Edit `Lambda timeout` to give enough time for your program to run

- [x] Copy `Boto3 Config client` [here](https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/config.html#client) and the response for `get_compliance_details_by_config_rule()` [here](https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/config.html#ConfigService.Client.get_compliance_details_by_config_rule) 
    - [x] Paste them in `Lambda` to access `Config` compliance report

- [x] Solution for error `"Unable to marshal response: Object of type datetime is not JSON serializable"` [here](https://stackoverflow.com/questions/61758398/aws-lambda-unable-to-marshal-response-error)

- [x] Filter the results of the `Config` response to get `resource ids` for the non compliant buckets

- [x] Create an `IAM Policy` (write action - `PutEncryptionConfiguration`) to allow `Lambda` to change `s3 encryption`
    - [x] attach it to the `existing role`

- [x] Copy `Boto3 s3 client` [here](https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/s3.html#client) and the response for `put_bucket_encryption` [here](https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/s3.html#S3.Client.put_bucket_encryption) 
    - [x] Paste in `Lambda` to apply encryption to the non compliant buckets

- [x] Create an `IAM Policy` (write action - `Publish`) to allow `Lambda` publish to an `SNS Topic` and attach it to the `existing role`

- [x] Create an `SNS Topic` that will send an `email`, create a `subscription` for it. 

- [x] Copy `Boto3 SNS client` [here](https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/sns.html#client) and the response for `publish` [here](https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/sns.html#SNS.Client.publish)
    - [x] Paste in `Lambda` to publish a message to the `SNS Topic` once the `s3` buckets are remediated.

- [x] Create a `EventBridge Rule` to trigger your `Lambda` function. The rule should trigger `Lambda` when `Config` reports any compliane changes
    - [x] Go to `EventBridge Console` > `Rules` > `Create Rule` > `Event Pattern` > `Predefined Pattern` > Select `AWS Service` > `Config` > `Config Rules Compliance Change` > `Specific Message` > `ComplianceChangeNotification` > `Specific Rule Name` > Select the `Config Rule` chosen earlier > Select your `Lambda Function` as `target` > Refresh your Lambda function and see the trigger has been added.

- [x] Create a new bucket `without encryption` and see if it gets remediated automatically
- [x] Delete all resources, roles, policies

## Via CLI
- [ ] Run this script [coming soon][#]

## Notes
- All services should be in `same region`
- For 10 tests, remediation was completed between 1 & 3 minute
- The Eventbridge trigger will trigger the lambda function based on compliance change, so when the compliance status changes back to `COMPLIANT` the lambda function will still be triggered. The code includes a condition to do nothing when there are no `NON-COMPLIANT` resources. However, the lambda functions till runs and there is a cost for that run time. 
- `AWS Config` is also capable of `autoremediation` using `SSM` docs, might be faster, and will elimitae the extra lambda cost, because it will only trigger when there are non-compliant resources


<br>
<br>

# Relevant Links
- [go to top](#contents)
- [Boto3 Docs](https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/index.html)
- [AWS Config Docs](https://docs.aws.amazon.com/config/latest/developerguide/WhatIsConfig.html)
- [Trigger Lambda with Config rule](https://mng.workshop.aws/config/configrule-with-lambda.html)
- Relevant CLI -
- Stack Overflow: [Find unencryped objects](https://stackoverflow.com/questions/54480431/how-to-find-un-encrypted-file-in-amazon-aws-s3-bucket), [lambda function to enable default encryption](https://stackoverflow.com/questions/69559813/aws-lambda-function-to-enable-default-encryption-on-bucket-creation), [Lambda to re-encrypt s3](https://joshua-hull.github.io/aws/python/2017/05/05/lambda-function-and-encrypted-s3.html)

## Other Reading
- [AWS Config Lambda Rules]( https://github.com/awslabs/aws-config-rules/blob/master/python/s3_bucket_default_encryption_enabled.py)
- [Auto enable s3 encryptio with config auto remediation - CLoud Formation/CLI](https://asecure.cloud/a/ar_ssm_s3_bucket_encryption/)
- [AWS COnfig s3 Auto Remidiation via Systems Manager](https://aws.amazon.com/blogs/mt/aws-config-auto-remediation-s3-compliance/), [Also](https://mklein.io/2021/01/06/bucket-compliance-and-remediation-with-aws-config/)
- [Cloudwatch & AWS Config & SNS](https://aws.amazon.com/premiumsupport/knowledge-center/config-resource-non-compliant/)
- [s3: Uploading Objects](https://docs.aws.amazon.com/AmazonS3/latest/userguide/upload-objects.html)
- [s3: Monitoring default encryption with Cloud Trail & CloudWatch](https://docs.aws.amazon.com/AmazonS3/latest/userguide/bucket-encryption-tracking.html)
- [s3: Prevent Unencrypted Uploads to S3](https://aws.amazon.com/blogs/security/how-to-prevent-uploads-of-unencrypted-objects-to-amazon-s3/)
- [s3: Encrypt Existing Unencrypted s3 Objects](https://aws.amazon.com/blogs/storage/encrypting-existing-amazon-s3-objects-with-the-aws-cli/)
- [s3 Inventory Docs](https://docs.aws.amazon.com/AmazonS3/latest/userguide/storage-inventory.html)
- [s3 Batch Docs](https://docs.aws.amazon.com/AmazonS3/latest/userguide/batch-ops.html)
- [Encrypting objects with s3 Batch Operations](https://aws.amazon.com/blogs/storage/encrypting-objects-with-amazon-s3-batch-operations/), [Also](https://spin.atomicobject.com/2020/09/15/aws-s3-encrypt-existing-objects/)
- [Re-encrypt s3 Objetcs with Batch, Lambda, Athena & s3 Inventory](https://aws.amazon.com/blogs/security/how-to-retroactively-encrypt-existing-objects-in-amazon-s3-using-s3-inventory-amazon-athena-and-s3-batch-operations/), [Also](https://kms-encryption-at-rest.workshop.aws/s3-track/module-2.html)
- [Automate s3 encryption w Terraform, Lambda, SNS & Cloudwatch](https://hervekhg.medium.com/how-to-automatically-enforce-default-encryption-on-newly-s3-bucket-using-terraform-through-aws-f91c82b7ca3a)

<br>
<br>

# Future Plans
- [go to top](#contents)
- [ ] Perform remediation via Config auto-remediate using SSM docs
- [ ] Remediate unencrypted buckets in and s3 as well as bucket encryption
- [ ] Remediate public s3 buckets and object, make them private
- [ ] Perform a remidiation to change all s3 bucket encryptiion from SSE-s3 to SSE-KMS (you might need to have more control over your encryption)
- [ ] Automaticall Add a tag to non compliant buckets, (remediate based on tags?) and change tags when they are compliant
- [ ] Deploy remediation via with Cloudformation and/or Terraform
- [ ] Deploy remediation via CLI
- [ ] Can you Prevent the creation of a bucket without default encryption enabled in the first place?

